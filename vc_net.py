#REQUIRES PYTHON3.6

import socket
import threading
import communicator
import pyaudio

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 40

HOST = '34.73.192.186'
PORT = 65432

p = pyaudio.PyAudio()

into = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

out = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                output=True,
                frames_per_buffer=CHUNK)

def listen(s,comm):
    while not comm.flag:
        data = into.read(CHUNK)
        s.sendall(data)
def p(comm):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        s.settimeout(.5)
        t = threading.Thread(target=listen, args=(s, comm))
        t.start()        
        while True:
            try:
                data = s.recv(1024)
                if not data:
                    print("what")
                    comm.flag = True
                    break
                out.write(data)
            except socket.timeout:
                if comm.flag:
                    print("Exiting Thread")
                    break
            except socket.error:
                print("wow")
                comm.flag = True
                    


print("Enter a username: ")
i = input()
comm = communicator.Communicator()
t = threading.Thread(target=p, args=(comm,))
t.start()
while i and not comm.flag:
    comm.append(i)
    i = input()
comm.flag = True