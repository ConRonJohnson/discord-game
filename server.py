#!/usr/bin/env python3

import socket
import threading
import communicator
import client

def client_listener(conn,c):
	while not c.flag:
		for mes in c.into:
			print("New mes, ",repr(mes))
			if not (type(mes) == bytearray):
				mes = bytearray(mes,'utf-8')
			conn.sendall(mes)
def client_handler(conn,a,c):
	with conn:
		conn.settimeout(1)
		print('Connected by', a)
		t = threading.Thread(target=client_listener, args=(conn,c))
		t.start()
		while True:
			try:
				data = conn.recv(1024)
				if not data:
					break
				if not c.name:
					c.name = data.decode('utf-8')
					print("New user: ",c.name)
					c.out.append("user "+c.name)
				else:
					c.out.append(data)
			except socket.timeout:
				if c.flag:
					print("Closing client thread")
					break
	c.out.append(None)

def send_mes (connections, mes, command, *args ):
	if command == 'user':
		for conn in connections:
			if conn.name == args[0]:
				conn.into.append(mes)
				break
	elif command == 'all':
		for conn in connections:
			conn.into.append(mes)
	elif command == 'exclude':
		for conn in connections:
			if not conn.name == args[0]:
				conn.into.append(mes)

def server(comm):
	HOST = '0.0.0.0'
	PORT = 65432
	connections= []
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.bind((HOST,PORT))
		print("Server Started")
		s.settimeout(1)
		s.listen()
		while True:
			try:
				conn, addr = s.accept()
				c = client.Client()
				connections.append(c)
				t = threading.Thread(target=client_handler, args=(conn, addr, c))
				t.start()
			except socket.timeout:
				#Check to see if this thread has been flagged to stop
				if comm.flag:
					print("exiting thread")

					#Flag client threads for termination
					for con in connections:
						con.flag = True
					break

				#Check for messages into this thread
				for mes in comm:
					args = mes.split()
					if args[0] == "send":
						if args[1] == "all":
							send_mes(connections, ' '.join(args[2:]),"all")
						if args[1] == "user":
							send_mes(connections, ' '.join(args[3:]),"user",args[2])
						if args[1] == "exclude":
							send_mes(connections, ' '.join(args[3:]),"exclude",args[2])


				#Check for messages from client threads
				for con in connections:
					for mes in con.out:
						if mes:
							if type(mes) == str:
								args = mes.split()
								if args[0] == "user":
									mes = "New User: " + ' '.join(args[1:])
								mes = bytearray(mes, 'utf-8')

							else:
								mes = mes.decode('utf-8')
								mes = con.name + "> " + mes
								mes = bytearray(mes, 'utf-8')
							print(mes)
							for send in connections:
								if not send == con:
									print("sending ",mes," to ", send.name)
									send.into.append(mes)
						else:
							print(con.name," disconnected")
							connections.remove(con)


inp = communicator.Communicator()
t = threading.Thread(target=server, args=(inp,))
t.start()
print("started thread")
a = input()
while a:
	inp.append(a)
	a = input()
print("Exiting program")
inp.flag = True
