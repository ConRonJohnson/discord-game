import tkinter as tk
import communicator
import net
import threading

root = tk.Tk()

text = tk.Text(root,bg='black',fg='white')
text.bind("<Key>", lambda e: "break")
text.insert(tk.END,"Enter A Username: ")
text.pack(expand=True,fill=tk.BOTH)

entry = tk.Entry(root)


def add_text(msg):
    comm.append(entry.get())
    
    text.insert(tk.END,'\n' + entry.get())
    text.see(tk.END)
    
    entry.delete(0,tk.END)

entry.bind("<Return>", add_text)
entry.pack(side = tk.BOTTOM,fill=tk.X)

comm = communicator.Communicator()
t = threading.Thread(target=net.p, args=(comm,lambda x: text.insert(tk.END, '\n' + x.decode('utf-8'))))
t.start()

root.mainloop()

comm.flag = True