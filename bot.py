import threading
import discord
import re
import game
import communicator
import asyncio

class Bot(discord.Client):
    def __init__(self,*args, **kwargs):
        super().__init__()
    def run(self,communicator):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        self.output = communicator
        super().run()
    async def on_ready(self):
        print('logged in as')
        print(self.user.name)
        print(self.user.id)
    async def on_message(self,msg):
        if msg.author == self.user:
            return
        if msg.content.startswith("!"):
            self.output.append(msg)
    def send(self,channel,message):
        self.bg_task = self.loop.create_task(channel.send(message))
        
def parse(mes: str) -> tuple:
    """
    formats $mes into a tuple that contains the command, a dict of options, and a list of arguments. 
    
    $mes FORMAT:
    <command> (*args) (-option) (-option=value) (-option=value1,value2) (*args)
    Where <> represents required and () represents optional.
    NOTE: $mes may not be space delimited, and *args can appear before or after options
    -options will begin with a "-" and may or may not have an associated value(s)
    NOTE: $mes does NOT contain the command prefix, which will be included in the output if it remains apart of $mes
    
    $out FORMAT:
    out = (
        command,
        {
            option: None,
            option2: (value),
            option3: (value1,value2),
            optionN: (value1, value2, ..., valueN),
        },
        [
            arg1,
            arg2,
            agr3,
            ...,
            argN
        ]
    )
    """
    words = mes.split()
    command = words[0]
    words = words[1:]
    
    words = ' '.join(words)
    
    pat = re.compile(r'-[a-zA-Z]\w*(?:\s*[=,]\s*\w+)*')
    #pat = re.compile(r'-[a-zA-Z]\w*(?:\s*=\s*(?:\s*,?\s*\w+)*)*')
    
    options = {}
    for op in pat.findall(words):
        mods = re.findall(r'\w[\w\s]*',op)
        if mods:
            if len(mods) > 1:
                options[mods[0].strip()] = tuple(mods[1:])
            else:
                options[mods[0].strip()] = None
    
        words = words.replace(op,"")
    return (command, options, words.split())
        


def start(g: game.Game):
    '''
    Populate $g with all of the different players (Creating Player objects as it goes)
    Create a dictionary of all the different players and their associated dmchannel (use the user.dm_channel and await user.create_dm() if necessary)
    '''

async def message_handler(client, inp: communicator.Communicator):
    await client.wait_until_ready()
    while not client.is_closed:
        print("a")
        for mes in inp:
            print(mes)
            mes[0].send(mes[1])

def run(out: communicator.Communicator, inp: communicator.Communicator):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop) #allows bot to run on a seperate thread
    token = "NjIxNTIxNzY0MzEyNDE2Mjg3.XXmjmQ.EngNDjaK08q7HJF-zTtpAW7P4so"
    
    global client
    client = discord.Client()
    
    general = None
    target = None
    
    @client.event
    async def on_message(message):
        global general
        global target
        if message.author == client.user:
            return
        
        if message.content.startswith("!"):
            out.append(message)
            #com.append(parse(message.content[1:]))
            #command, options, args = parse(message.content[1:])
            #if command == "start":
            #    pass
            #elif command == "move":
            #    pass
            #elif command == "say":
            #    await message.channel.send(' '.join(args),delete_after=60)
    
    @client.event
    async def on_ready():
        global general
        global target
        print('logged in as')
        print(client.user.name)
        print(client.user.id)
        general = client.get_channel(621521035279335449)
        target = client.get_channel(621525626913947649)
        
        
    client.loop.create_task(message_handler(client, inp))
    client.run(token)
#run()