import socket
import threading
import communicator

HOST = '34.73.192.186'
PORT = 65432


def listen(s,comm):
    while not comm.flag:
        for mes in comm:
            print ("'new mes, ",mes)
            if not type(mes) == bytearray:
                mes = bytearray(mes,'utf-8')
            s.sendall(mes)
            print("sent")
def p(comm, func=None):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        s.settimeout(.5)
        t = threading.Thread(target=listen, args=(s, comm))
        t.start()        
        while True:
            try:
                data = s.recv(1024)
                if not data:
                    print("what")
                    comm.flag = True
                    break
                if func:
                    print("received")
                    func(data)
                else:
                    print(data.decode('utf-8'))
            except socket.timeout:
                if comm.flag:
                    print("Exiting Thread")
                    break


#print("Enter a username: ")
#i = input()
#comm = communicator.Communicator()
#t = threading.Thread(target=p, args=(comm,))
#t.start()
#while i and not comm.flag:
#    comm.append(i)
#    i = input()
#comm.flag = True