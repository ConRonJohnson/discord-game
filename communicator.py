class Communicator():
    def __init__(self):
        self.queue = []
        self.flag = False
        self.name = None
    def __iter__(self):
        return self
    def __next__(self):
        if len(self.queue) > 0:
            ret = self.queue[0]
            del self.queue[0]
            return ret
        else:
            raise StopIteration
    def append(self, value):
        self.queue.append(value)
