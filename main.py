import bot
import entity
import player
import re
import communicator
import threading
import asyncio
import game
import time

inp = communicator.Communicator()
out = communicator.Communicator()
thread = threading.Thread(target=bot.run, args=(inp, out))
thread.start()
prevTime = time.clock()
count = 5.0
while True:
    for mes in inp:
        channel = mes.channel
        cont = str(bot.parse(mes.content))
        #print(cont)
        out.append((channel,cont))
    curTime = time.clock()
    dt = curTime - prevTime
    prevTime = curTime
    count -= dt
    